nominatim_url <- "http://nominatim.openstreetmap.org/search"

nominatim_get <- function(query, countrycode="us") {
    xmlParse(RCurl::getForm(
        nominatim_url,
        q=query,
        countrycodes=countrycode,
        format="xml"))
}

#' Search for a location using OpenStreetMap Nominatim
#'
#' \href{http://nominatim.openstreetmap.org}{Nominatim} is a web
#' service that can search for a location (latitude and longitude) by
#' name or description. This function queries it, and returns results
#' ordered by best match.
#'
#' By default, the search is restricted to locations within the United
#' States, as the weather data we have available is restricted to the
#' United States.
#'
#' The output is a data frame, with columns for a human-readable
#' description, and a latitude and longitude.
#'
#' The Nominatim web service is provided free of charge by
#' OpenStreetMap.org.  It is the responsibility of the client of this
#' function to adhere to the
#' \href{http://wiki.openstreetmap.org/wiki/Nominatim_usage_policy}{usage policy}.
#'
#' @param query a textual query
#' @param countrycode Results will be restricted to this country
#' @return a data frame, with columns Description, Latitude, Longitude
#' @export
nominatim_search <- function(query, countrycode="us") {
    x <- nominatim_get(query, countrycode)
    places <- XML::getNodeSet(x, "//place")
    desc <- sapply(places, xmlGetAttr, "display_name")
    desc <- stringr::str_replace(desc, ", United States of America", "")
    data.frame(
        Description = desc,
        Latitude = as.numeric(sapply(places, xmlGetAttr, "lat")),
        Longitude = as.numeric(sapply(places, xmlGetAttr, "lon")))
}
