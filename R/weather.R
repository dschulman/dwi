#' Compute relative humidity
#'
#' @param temp Dry bulb temperature, in Farenheit
#' @param dewpoint Dewpoint temperature, in Farenheit
#' @return Relative humidity, in percentage
#' @export
humidity <- function(temp, dewpoint) {
    stopifnot(length(temp) == length(dewpoint))
    t_c <- (5/9) * (temp - 32)
    dp_c <- (5/9) * (dewpoint - 32)
    beta <- (112 - (0.1 * t_c) + dp_c)/(112 + (0.9 * t_c))
    return (100 * beta^8)
}

#' Convert knots to miles per hour
#'
#' @param knots A (wind) speed in knots
#' @return The equivalent mph
#' @export
knots_to_mph <- function(knots) knots*1.15078

#' Compute (approximate) hours of daylight
#'
#' Given a location (latitude and longitude) and a date, compute the
#' approximate hours of daylight, using the NOAA algorithms.  A vector
#' of dates can be given to compute multiple results at once.
#'
#' @param lat Latitude, in degrees
#' @param lon Longitude, in degrees. West is negative.
#' @param dates A vector of dates.
#' @return A numeric vector
#' @export
daylight <- function(lat, lon, dates) {
    loc <- matrix(c(lon, lat), nrow=1)
    dates <- as.POSIXct(dates)
    sunrise <- maptools::sunriset(
        loc, dates,
        direction = 'sunrise',
        POSIXct.out = TRUE)[,2]
    sunset <- maptools::sunriset(
        loc, dates,
        direction = 'sunset',
        POSIXct.out = TRUE)[,2]
    as.numeric(sunset - sunrise, units='hours')
}
