# DWI

DWI is a R package that implements the Daily Walking Index (DWI), a score that 
summarizes the predicted effect of local weather on daily walking and other 
moderate outdoor physical activity.

## Features

* Fetching weather forecasts (US only) from the [National Digital Forecast Database](http://www.nws.noaa.gov/ndfd/).
* Fetching historical weather (US only) from the NOAA [QCLCD](http://www.ncdc.noaa.gov/data-access/land-based-station-data/land-based-datasets/quality-controlled-local-climatological-data-qclcd) database.
* Computing the DWI.
* An example webapp using [Shiny](http://shiny.rstudio.com/) that visualizes the 
DWI, based on either forecast or historical weather.

## Installation

This packages is currently not on CRAN (the standard R package repository), and 
can most easily be installed from the source repository:

```r
if (!require('devtools')) install.packages('devtools')
devtools::install_bitbucket('dschulman/dwi')
```
